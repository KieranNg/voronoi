import math
import random
import pygame
import Settings
from rendering.Screen import Screen
from geometry.Vector import Vector
from Voronoi import VoronoiGenerator

# There might be a possible error if _x is 0
def GenerateRandomPoints(_length): #generate list of points, parameter determines how mant vectors you want to generate
    points = [Vector] * _length
    for i in range(len(points)):
        points[i] = Vector(
            math.floor(random.random() * Settings.SCREEN_WIDTH),
            math.floor(random.random() * Settings.SCREEN_HEIGHT))
    return points
    
def GenerateFixedPoints():
    points = [Vector] * 3
    points[0] = Vector(50, 150)
    points[1] = Vector(125, 325)
    points[2] = Vector(400, 400)

    return points

def main():
    #points = GenerateRandomPoints(Settings.NUMBER_OF_POINTS)
    points = GenerateFixedPoints()
    points.sort(key=lambda p: p.y)

    voronoi_generator = VoronoiGenerator(points)

    pygame.init()
    screen = Screen(voronoi_generator)
    screen.voronoi_painter.run()
    screen.display(Settings.SCREEN_WIDTH, Settings.SCREEN_HEIGHT)
    screen.update()

if __name__ == "__main__":
    main()