import copy
import Settings
from geometry.Breakpoint import Breakpoint
from data_structures.Tree import Tree
from geometry.Vector import Vector
from geometry.HalfEdge import HalfEdge
from geometry.Arc import Arc
from events.CircleEvent import CircleEvent
from events.SiteEvent import SiteEvent

class VoronoiGenerator:
    def __init__(self, points):
        self.event_queue = []
        self.sites = points
        self.beach_line = Tree()
        self.sweep_line = 0.0
        self.vertices = []
        self.edges = []
        self.circles = []

        self.initialize()

    def initialize(self):
        for s in self.sites:
            self.event_queue.append(SiteEvent(s))

    def generate_voronoi(self):
        self.initialize()

        while self.have_events():
            self.progress()
            
        self.complete()

    def have_events(self):
        return len(self.event_queue) > 0

    def progress(self):
        current_event = self.event_queue.pop(0)
        self.sweep_line = current_event.position.y + Settings.SWEEP_LINE_OFFSET

        if isinstance(current_event, SiteEvent) and current_event.active:
            self.handle_site_event(current_event)
            
        elif isinstance(current_event, CircleEvent) and current_event.active:
            self.handle_circle_event(current_event)

        current_event.active = False

        self.sort_queue()

    def handle_site_event(self, site_event):
        """
        Reference: https://www.cs.umd.edu/class/spring2020/cmsc754/Lects/lect11-vor.pdf
        Let p_i be the new site.
        1.  Advance the sweep line so that it passes through p_i
            Apply the above search operation to determine the beach line arc that lies immediately above p_i
            Let p_j be the corresponding site.
        2.  Applying the above insert-and-split operation, inserting a new entry for p_i, thus
            replacing <..., p_j, ...> with <..., p_j, p_i, p_j, ...>.
        3.  Create a new (dangling) edge in the Voronoi diagram, which lies on the bisector between p_i and p_j.
        4.  Some old triples that involved p_j may need to be deleted and some new triples
            involving p_i will be inserted, based on the change of neighbors on the beach line.
            Note that the newly created beach-line triple p_j, p_i, p_j does not generate an event
            because it only involves two distinct sites.
        """

        def update_half_edges(left_breakpoint, right_breakpoint):
            left_breakpoint.half_edge = HalfEdge(left_breakpoint.data)
            right_breakpoint.half_edge = HalfEdge(right_breakpoint.data)
            left_breakpoint.half_edge.twin = right_breakpoint.half_edge
            
            self.edges.append(left_breakpoint.half_edge)
            self.vertices.append(left_breakpoint.get_key(self.sweep_line))

        # Before: ... p1, p2, pj, p3, p4
        # After: ... p1, p2, pj*, pi, pj**, p3, p4

        # If the beach line is empty then we add this site as the root arc
        if self.beach_line.root is None:
            self.beach_line.root = Arc(site_event.position)
            return

        new_arc = Arc(site_event.position)
        left_breakpoint, right_breakpoint = self.beach_line.insert_arc(new_arc, self.sweep_line)
        
        # Now we create the new half edges
        update_half_edges(
            left_breakpoint,
            right_breakpoint
        )

        # Create for new circle events
        self.create_new_circle_events(
            [left_breakpoint.left.predecessor(), left_breakpoint.left, left_breakpoint.right],
            [left_breakpoint.right, right_breakpoint.right, right_breakpoint.right.successor()]
        )

    def handle_circle_event(self, circle_event):
        """
        Reference: https://www.cs.umd.edu/class/spring2020/cmsc754/Lects/lect11-vor.pdf
        Let p_i, p_j, and p_k be the three sites that generated this event, from left to right.
        1.  Delete the entry for p_j from the beach line status. (The associated arc)
        2.  Create a new vertex in the Voronoi diagram (at the circumcenter of {p_i, p_j, p_k}) and
            join the two Voronoi edges for the bisectors (p_i, p_j), (p_j , p_k) to this vertex.
        3.  Create a new (dangling) edge for the bisector between p_i and p_k.
        4.  Delete any events that arose from triples involving the arc of p_j, and generate new
            events corresponding to consecutive triples involving pi and pk.
        """

        # Given an arc, find the left and right breakpoints
        def find_arc_breakpoints(arc):
            left_breakpoint = None
            right_breakpoint = None
            if arc.is_left_child():
                # Then parent is right breakpoint
                right_breakpoint = arc.parent
                
                left_breakpoint = self.beach_line.find_breakpoint(
                    Breakpoint.intersection(
                        arc.predecessor().focus, 
                        arc.focus, 
                        self.sweep_line),
                    self.sweep_line)
            elif arc.is_right_child():
                # Then parent is left breakpoint
                left_breakpoint = arc.parent

                right_breakpoint = self.beach_line.find_breakpoint(
                    Breakpoint.intersection(
                        arc.focus, 
                        arc.successor().focus, 
                        self.sweep_line),
                    self.sweep_line)
            
            # We note that the new breakpoint is whichever is not the parent
            return left_breakpoint, right_breakpoint

        def update_half_edges(new_vertex, left_breakpoint, right_breakpoint, new_breakpoint):
            # Update the origin of both left and right breakpoints to be the circle midpoint
            # However, we need to get the correct twin.
            # We find the twin which has an origin equal to left_breakpoint or right_breakpoint
            if left_breakpoint.half_edge.get_origin(self.sweep_line) == new_vertex:
                left_breakpoint.half_edge.origin = new_vertex
            else:
                left_breakpoint.half_edge.twin.origin = new_vertex

            if right_breakpoint.half_edge.get_origin(self.sweep_line) == new_vertex:
                right_breakpoint.half_edge.origin = new_vertex
            else:
                right_breakpoint.half_edge.twin.origin = new_vertex
                
            new_half_edge = HalfEdge(new_breakpoint.data)
            new_half_edge.twin = HalfEdge(new_vertex)
            new_breakpoint.half_edge = new_half_edge
            
            # We set the next edges of each half edge

            # We finally add the new half edge into the edges list
            self.edges.append(new_half_edge)

            #new_breakpoint.half_edge = new_half_edge

        # Get the breakpoints before deleteing the arc
        arc = circle_event.arc
        arc_tuple = circle_event.arc_tuple

        # Create the new vertex
        new_vertex = circle_event.mid_point
        self.vertices.append(new_vertex)

        left_breakpoint, right_breakpoint = find_arc_breakpoints(arc)
        
        # Delete the arc and update the beachline
        self.beach_line.delete(arc)
        
        # Get new breakpoint formed
        new_breakpoint = self.beach_line.find_breakpoint(
            Breakpoint.intersection(
                left_breakpoint.data[0].focus, 
                right_breakpoint.data[1].focus, 
                self.sweep_line),
            self.sweep_line)

        # Update the half edges
        update_half_edges(new_vertex, left_breakpoint, right_breakpoint, new_breakpoint)
        
        # We remove events that involve event.arc
        for event in self.event_queue:
            if isinstance(event, CircleEvent):
                if arc in event.arc_tuple:
                    event.active = False
        
        # Create for new circle events (that do not involve arc / arc_tuple[1])
        self.create_new_circle_events(
            [arc_tuple[0].predecessor(), arc_tuple[0], arc_tuple[2]],
            [arc_tuple[0], arc_tuple[2], arc_tuple[2].successor()]
        )

    def create_new_circle_events(self, arc_tuple_A, arc_tuple_B):
        event_I = CircleEvent.check_for_circle_events(arc_tuple_A[0], arc_tuple_A[1], arc_tuple_A[2], self.sweep_line)
        event_J = CircleEvent.check_for_circle_events(arc_tuple_B[0], arc_tuple_B[1], arc_tuple_B[2], self.sweep_line)

        if event_I is not None:
            self.event_queue.append(event_I)
            self.circles.append([event_I.mid_point, event_I.radius])
        if event_J is not None:
            self.event_queue.append(event_J)
            self.circles.append([event_J.mid_point, event_J.radius])

    def sort_queue(self):
        self.event_queue.sort(key = lambda e: e.position.y)

    def complete(self):
        print(self.beach_line)
        self.sweep_line = 10000.0
        """
        Reference: https://pvigier.github.io/2018/11/18/fortune-algorithm-details.html
        To complete the diagram, we need to bound every vertex and edge within a minimally sized bounding box.
        We first get all the faces in the diagram
        Then we do the following:
        For each cell we traverse its half-edges and we check the intersection between this half-edge and the box. There are five cases:
        1.  The half-edge is completely inside the box (Both origin and destination lie inside the box): we keep this half-edge
        2.  The half-edge is completely outside the box (Both origin and destination lie outside the box): we discard this half-edge
        3.  The half-edge is going outside the box: we clip the half-edge and we store it as the last half-edge that went outside.
            The destination of the half edge is a point on the box
        4.  The half-edge is going inside the box: we clip the half-edge and we add half-edges to link it with the last half-edge that went outside (we store it at case 3 or 5)
            The origin of the half edge is a point on the box
        5.  The half-edge crosses the box twice: we clip the half-edge, we add half-edges to link it with the last half-edge that went outside,
            and we store it as the new last half-edge that went outside.
        """
        pass