import copy
from geometry.Breakpoint import Breakpoint
from geometry.Arc import Arc

class Tree:
    def __init__(self):
        self.root = None
    
    def find_breakpoint(self, breakpoint, sweep_line):
        def find(current_node):
            if not isinstance(current_node, Breakpoint):
                return current_node.parent

            key = current_node.get_key(sweep_line)

            if breakpoint == key:
                return current_node
            
            if breakpoint.x < key.x:
                return find(current_node.left)
            elif breakpoint.x > key.x:
                return find(current_node.right)

            # None of the above worked
            # Why? It is possible that both breakpoints have the same value
            # But the data values are swapped.
            # For example, Breakpoint(a, b) == Breakpoint(b, a)
            return find(current_node.left) or find(current_node.right)
                
        return find(self.root)


    def insert_arc(self, new_arc, sweep_line):
        # You might notice that it is possible for the left child to have a larger
        # x coordinate value compared to the right child.
        # This is because the tree does not necessarily care about the x coordinates of each site,
        # But how they appear on the beachline.
        current_node = self.root

        while isinstance(current_node, Breakpoint):
            breakpoint = current_node.get_key(sweep_line)
            if new_arc.focus.x < breakpoint.x:
                current_node = current_node.left
            else:
                current_node = current_node.right
        
        # current_node should be a leaf node
        left_breakpoint = Breakpoint(copy.deepcopy(current_node), new_arc)
        right_breakpoint = Breakpoint(left_breakpoint, copy.deepcopy(current_node))

        if current_node.is_left_child():
            current_node.parent.left = right_breakpoint
        elif current_node.is_right_child():
            current_node.parent.right = right_breakpoint
        elif current_node.parent is None:
            self.root = right_breakpoint
        
        self.validate_data(left_breakpoint)
        return left_breakpoint, right_breakpoint

    # Deletes the input arc from the tree and also validates all ancestor nodes
    def delete(self, arc):
        if arc is None:
            return
        
        grandparent = arc.delete()
        self.validate_data(grandparent)

    def validate_data(self, current_node):
        if current_node is None:
            return
        
        if isinstance(current_node, Breakpoint):
            current_node.data[0] = current_node.left.largest_child()
            current_node.data[1] = current_node.right.smallest_child()
       
        self.validate_data(current_node.parent)

    def in_order_traversal(self):
        def in_order_traversal(current_node, output):
            if current_node is not None:
                output = str(current_node) + ", "
                if isinstance(current_node, Breakpoint):
                    output += in_order_traversal(current_node.left, output)
                    output += in_order_traversal(current_node.right, output)
                return output
            else:
                return ""
        return in_order_traversal(self.root, "")

    def leaves(self):
        leaves = []
        def get_leaves(current_node):
            if current_node is None:
                return
            
            if isinstance(current_node, Breakpoint):
                get_leaves(current_node.left)
                get_leaves(current_node.right)
            elif isinstance(current_node, Arc):
                leaves.append(current_node)
        get_leaves(self.root)
        return leaves

    def __str__(self):
        return self.in_order_traversal()