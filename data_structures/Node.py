class Node:
    def __init__(self, data):
        self._data = data
        self._parent = None
        self._height = None
    
    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    def get_key(self, sweep_line):
        return self.data
    
    @property
    def parent(self):
        return self._parent
    
    @parent.setter
    def parent(self, value):
        self._parent = value

    @property
    def height(self):
        def calculate_height():
            count = 0
            current_node = self.parent
            while current_node is not None:
                current_node = current_node.parent
                count += 1
            return count
        
        return calculate_height()

    @height.setter
    def height(self, value):
        self._height = value
    
    def is_left_child(self):
        if self.parent is None:
            return False
        return self.parent.left == self

    def is_right_child(self):
        if self.parent is None:
            return False
        return self.parent.right == self

    def smallest_child(self):
        return self
    
    def largest_child(self):
        return self

    # Largest value smaller than the node value
    def predecessor(self):
        # We walk up and right until we are no longer a left child
        current_node = self
        while current_node.is_left_child():
            current_node = current_node.parent
        
        # current_node should be a right child
        if current_node.parent is None or current_node.parent.left is None:
            return None
        
        return current_node.parent.left.largest_child()
    
    # Smallest value larger than the node value
    def successor(self):
        # We walk up and left until we are no longer a right child
        current_node = self
        while current_node.is_right_child():
            current_node = current_node.parent
        
        # current_node should be a left child
        if current_node.parent is None or current_node.parent.right is None:
            return None
        
        return current_node.parent.right.smallest_child()

    def __str__(self):
        return str(self.data)