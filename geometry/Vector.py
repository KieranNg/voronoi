import math

class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @staticmethod
    def is_close(a, b):
        return math.isclose(a.x, b.x) and math.isclose(a.y, b.y)
    
    @staticmethod
    def mid_point(a, b):
        return Vector((a.x + b.x) / 2.0, (a.y + b.y) / 2.0)
    
    @staticmethod
    def distance(a, b):
        return math.sqrt((a.x - b.x) ** 2.0 + (a.y - b.y) ** 2.0)
    
    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"

    def __eq__(self, other):
        return isinstance(other, Vector) and math.isclose(self.x, other.x) and math.isclose(self.y, other.y)

    def to_tuple(self):
        return (self.x, self.y)