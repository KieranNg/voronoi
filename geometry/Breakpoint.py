import math
from data_structures.Node import Node
from geometry.Vector import Vector

class Breakpoint(Node):
    def __init__(self, left, right):
        super().__init__([left.largest_child(), right.smallest_child()])
        self._left = left
        self._right = right

        left.parent = self
        right.parent = self

        self.half_edge = None

    def get_key(self, sweep_line):
        return self.get_breakpoint(sweep_line)
    
    def get_breakpoint(self, sweep_line):
        point = self.data[0].focus
        result = Vector(0.0, 0.0)

        a = self.data[0].focus.x
        b = self.data[0].focus.y
        p = self.data[1].focus.x
        q = self.data[1].focus.y
        c = sweep_line

        if b == c:
            result.x = a
            point = self.data[1].focus
        elif q == c:
            result.x = p
        elif b == q:  # Same height
            result.x = (a + p) / 2.0

            if p < a:
                result.y = float("inf")
                return result
        else:
            x1 = (-math.sqrt((-(b*c)+(b*q)+c**2-(c*q))*(a**2-2*(a*p)+b**2-2*(b*q)+p**2+q**2)) + (a*c)-(a*q)+(b*p)-(c*p)) / (b-q)
            x2 = (math.sqrt((-(b*c)+(b*q)+c**2-(c*q))*(a**2-2*(a*p)+b**2-2*(b*q)+p**2+q**2)) + (a*c)-(a*q)+(b*p)-(c*p)) / (b-q)
            
            # Suppose vertices n, m and breakpoints b1 and b2.
            # Then b1 = (n, m) but b2 = (m, n)
            # Such breakpoints can both exist in the tree
            # Hence we need to ensure that both breakpoints will give a different value
            # So, if the x coordinate of n < x coordinate of m, we return the left breakpoint
            # Else we return the right breakpoint
            # Note that we assume the right breakpoint has a larger x value
            if a < p:
                result.x = min(x1, x2)
            else:
                result.x = max(x1, x2)
        
        a = point.x
        b = point.y
        x = result.x
        h = 2 * (b - c)

        if h == 0:
            result.y = float("inf")
            return result
        
        result.y = (1 / h) * (x**2 - (2 * a * x) + a**2 + b**2 - c**2)
        #print("Breakpoint between", self.data[0], self.data[1], "at", sweep_line, ":", result)
        return result

    @property
    def left(self):
        return self._left
    
    @left.setter
    def left(self, node):
        if node is not None:
            node.parent = self
            self.data[0] = node.largest_child() 
        else:
            self.data[0] = None
        self._left = node
    
    @property
    def right(self):
        return self._right

    @right.setter
    def right(self, node):
        if node is not None:
            node.parent = self
            self.data[1] = node.smallest_child()
        else:
            self.data[1] = None
        self._right = node
    
    def smallest_child(self):
        current_node = self
        if current_node is None:
            return self

        while isinstance(current_node, Breakpoint) and current_node.left is not None:
            current_node = current_node.left
        return current_node
    
    def largest_child(self):
        current_node = self
        if current_node is None:
            return self

        while isinstance(current_node, Breakpoint) and current_node.right is not None:
            current_node = current_node.right
        return current_node

    # Largest value smaller than the node value
    def predecessor(self):
        if isinstance(self, Breakpoint) and self.left is not None:
            return Node.smallest_child(self)
        
        # We walk up and right until we are no longer a left child
        current_node = self
        while current_node.is_left_child():
            current_node = current_node.parent
        
        # current_node should be a right child
        if current_node.parent is None or current_node.parent.left is None:
            return None
        
        return Node.largest_child(current_node.parent.left)
    
    # Smallest value larger than the node value
    def successor(self):
        if isinstance(self, Breakpoint) and self.right is not None:
            return Node.largest_child(self)
        
        # We walk up and left until we are no longer a right child
        current_node = self
        while current_node.is_right_child():
            current_node = current_node.parent
        
        # current_node should be a left child
        if current_node.parent is None or current_node.parent.right is None:
            return None
        
        return Node.smallest_child(current_node.parent.right)

    @staticmethod
    def intersection(focus_A, focus_B, sweep_line):
        point = focus_A
        result = Vector(0.0, 0.0)

        a = focus_A.x
        b = focus_A.y
        p = focus_B.x
        q = focus_B.y
        c = sweep_line

        if b == c:
            result.x = a
            point = focus_B.focus
        elif q == c:
            result.x = p
        elif b == q:  # Same height
            result.x = (a + p) / 2.0

            if p < a:
                result.y = float("inf")
                return result
        else:
            x1 = (-math.sqrt((-(b*c)+(b*q)+c**2-(c*q))*(a**2-2*(a*p)+b**2-2*(b*q)+p**2+q**2)) + (a*c)-(a*q)+(b*p)-(c*p)) / (b-q)
            x2 = (math.sqrt((-(b*c)+(b*q)+c**2-(c*q))*(a**2-2*(a*p)+b**2-2*(b*q)+p**2+q**2)) + (a*c)-(a*q)+(b*p)-(c*p)) / (b-q)
            
            # Suppose vertices n, m and breakpoints b1 and b2.
            # Then b1 = (n, m) but b2 = (m, n)
            # Such breakpoints can both exist in the tree
            # Hence we need to ensure that both breakpoints will give a different value
            # So, if the x coordinate of n < x coordinate of m, we return the left breakpoint
            # Else we return the right breakpoint
            # Note that we assume the right breakpoint has a larger x value
            if a < p:
                result.x = min(x1, x2)
            else:
                result.x = max(x1, x2)
        
        a = point.x
        b = point.y
        x = result.x
        h = 2 * (b - c)

        if h == 0:
            result.y = float("inf")
            return result
        
        result.y = (1 / h) * (x**2 - (2 * a * x) + a**2 + b**2 - c**2)
        return result

    def __str__(self):
        return "(" + str(self.data[0]) + ", " + str(self.data[1]) + ")"

    def __eq__(self, other):
        return isinstance(other, Breakpoint) and self.data[0] == other.data[0] and self.data[1] == other.data[1]