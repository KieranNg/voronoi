from data_structures.Node import Node

class Arc(Node):
    def __init__(self, focus):
        super().__init__(focus)
        self.circle_event = None

    @property
    def focus(self):
        return self.data
    
    @focus.setter
    def focus(self, value):
        Node.data.fset(value)

    # Deletes this arc and replaces parent with other child arc
    def delete(self):
        parent = self.parent
        grandparent = parent.parent
        
        if self.is_left_child():
            # Replace grandparent's child node with right child
            if parent.is_left_child():
                grandparent.left = parent.right
            elif parent.is_right_child():
                grandparent.right = parent.right
        elif self.is_right_child():
            # Replace grandparent's child node with left child
            if parent.is_left_child():
                grandparent.left = parent.left
            elif parent.is_right_child():
                grandparent.right = parent.left
        
        return grandparent

    def get_key(self, sweep_line):
        return super().get_key(sweep_line)
    
    def get_value(self, x, sweep_line):
        u = (self.focus.y - sweep_line) * 2
        v = (x ** 2 - 2 * self.focus.x * x + self.focus.x ** 2 + self.focus.y ** 2 - sweep_line ** 2)
        return v /u
        
    def __str__(self):
        return str(self.focus)

    def __eq__(self, other):
        return isinstance(other, Arc) and self.data == other.data