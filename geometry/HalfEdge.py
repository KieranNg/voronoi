import copy
from geometry.Breakpoint import Breakpoint

class HalfEdge:
    # Note that origin is a vector while destination is a breakpoint unless it is completed
    def __init__(self, origin):
        # Deep copy here because breakpoint can be adjusted but we need this to remain the same
        self.origin = copy.deepcopy(origin)

        self._twin = None

        self._next = None
        self._prev = None

        self.active = True

    def get_origin(self, sweep_line):
        if isinstance(self.origin, list):
            return Breakpoint.intersection(self.origin[0].focus, self.origin[1].focus, sweep_line)
        else:
            return self.origin

    def get_destination(self, sweep_line):
        if self._twin is not None:
            return self._twin.get_origin(sweep_line)
    
    @property
    def twin(self):
        return self._twin
    
    @twin.setter
    def twin(self, other):
        self._twin = other
        other._twin = self
    
    @property
    def next(self):
        return self._next
    
    @next.setter
    def next(self, other):
        self._next = other
        other._prev = self
    
    @property
    def prev(self):
        return self._prev

    @prev.setter
    def prev(self, other):
        self._prev = other
        other._next = self