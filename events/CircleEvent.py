import math
from geometry.Vector import Vector
from events.Event import Event

class CircleEvent(Event):
    def __init__(self, position, mid_point, radius, arc_tuple):
        super().__init__(position)
        self.mid_point = mid_point
        self.radius = radius
        self.arc_tuple = arc_tuple

    @property
    def arc(self):
        return self.arc_tuple[1]
    
    @staticmethod
    def check_for_circle_events(arcA, arcB, arcC, sweep_line):
        if arcA is None or arcB is None or arcC is None:
            return None

        mid_point, radius = CircleEvent.create_circle(arcA.focus, arcB.focus, arcC.focus)

        if mid_point is not None and radius is not None:
            lowest_point = Vector(mid_point.x, mid_point.y + radius)
            if lowest_point.y > sweep_line:

                return CircleEvent(lowest_point, mid_point, radius, (arcA, arcB, arcC))

        return None
    
    @staticmethod
    def create_circle(a, b, c):
        A = b.x - a.x
        B = b.y - a.y
        C = c.x - a.x
        D = c.y - a.y
        E = A * (a.x + b.x) + B * (a.y + b.y)
        F = C * (a.x + c.x) + D * (a.y + c.y)
        G = 2 * (A * (c.y - b.y) - B * (c.x - b.x))

        if G == 0:
            return None, None

        mid_point = Vector((D * E - B * F) / G, (A * F - C * E) / G)
        radius = math.sqrt((a.x - mid_point.x) ** 2 + (a.y - mid_point.y) ** 2)

        return mid_point, radius