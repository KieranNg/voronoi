import sys
import math
import copy
import pygame.display
import Settings

class Screen:
    """Renderer

    Attributes
    ----------
    complete: Boolean
        Whether the renderer should stop updating
    clock: Clock
        The clock used to update
    """
    def __init__(self, voronoi_generator):
        """Constructor"""
        pygame.display.init()
        self.voronoi_painter = VoronoiPainter(voronoi_generator)
        self.complete = False
        self.clock = pygame.time.Clock()

    def display(self, width, height, caption = "Renderer"):
        """Display Function

        Parameters
        ----------
        _width: float
            The horizontal length of the screen
        _height: float
            The vertical length of the screen
        _caption: String
            The window title
        """
        pygame.display.set_mode((width, height))
        pygame.display.set_caption(caption)

    def update(self):
        """Update Function"""
        self.voronoi_painter.draw_voronoi()

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT or \
                (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT or event.key == pygame.K_UP:
                        self.voronoi_painter.next()
                    if event.key == pygame.K_LEFT or event.key == pygame.K_DOWN:
                        self.voronoi_painter.prev()
            
            pygame.display.update()
            self.clock.tick(60)

class VoronoiPainter:
    def __init__(self, voronoi_generator):
        self.voronoi_generator = voronoi_generator
        self.states = []
        self.count = 0

    def progress(self):
        self.voronoi_generator.progress()
        self.states.append([
            copy.deepcopy(self.voronoi_generator.beach_line),
            copy.deepcopy(self.voronoi_generator.edges),
            copy.deepcopy(self.voronoi_generator.vertices),
            self.voronoi_generator.sweep_line,
            copy.deepcopy(self.voronoi_generator.circles)])

    def complete(self):
        self.voronoi_generator.complete()
        self.states.append([
            copy.deepcopy(self.voronoi_generator.beach_line),
            copy.deepcopy(self.voronoi_generator.edges),
            copy.deepcopy(self.voronoi_generator.vertices),
            self.voronoi_generator.sweep_line,
            copy.deepcopy(self.voronoi_generator.circles)])

    def run(self):
        while self.voronoi_generator.have_events():
            self.progress()
        self.complete()

    def next(self):
        if self.count < len(self.states) - 1:
            self.count += 1
            self.draw_voronoi()

    def prev(self):
        if self.count > 0:
            self.count -= 1
            self.draw_voronoi()

    def draw_voronoi(self):
        surface = pygame.display.get_surface()
        curr_state = self.states[self.count]

        surface.fill(pygame.Color(0, 0, 0))

        for leaf in curr_state[0].leaves():
            self.draw_parabola(leaf, surface, 1000, 1, curr_state[3])

        for edge in curr_state[1]:
            if edge.twin is not None:
                pygame.draw.line(
                    surface,
                    pygame.Color(0, 255, 0),
                    edge.get_origin(curr_state[3]).to_tuple(),
                    edge.get_destination(curr_state[3]).to_tuple(),
                    1)

        for vertex in curr_state[2]:
            pygame.draw.ellipse(
                surface,
                pygame.Color(255, 125, 125),
                pygame.Rect(vertex.x, vertex.y, 4.0, 4.0))

        pygame.draw.line(surface,
            pygame.Color(100, 100, 100),
            (0, curr_state[3]),
            (Settings.SCREEN_WIDTH, curr_state[3])
            )
        
        for point in self.voronoi_generator.sites:
            pygame.draw.ellipse(
                surface,
                pygame.Color(255, 0, 0),
                pygame.Rect(point.x, point.y, 4.0, 4.0))
            
        for circle in curr_state[4]:
            pygame.draw.circle(surface, 
                pygame.Color(0, 100, 200), 
                circle[0].to_tuple(), 
                circle[1],
                width=1)
        
    def draw_parabola(self, arc, surface, resolution, increment, directrix):
        xPos = arc.focus.x - (math.floor(resolution / 2) * increment)
        for i in range(resolution):
            pygame.draw.line(
                surface,
                pygame.Color(125, 125, 0),
                (xPos, arc.get_value(xPos, directrix)),
                (xPos + increment, arc.get_value(xPos + increment, directrix)))
            xPos += increment